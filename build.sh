#!/usr/bin/env bash

# Show some tools information
id
pwd
buildah --version
podman --version
skopeo --version
ls -l /

# Pull the latest fedora image
podman pull registry.fedoraproject.org/fedora:latest

# Find its version
export $(
    podman run registry.fedoraproject.org/fedora grep VERSION_ID /etc/os-release
)
echo ${VERSION_ID}

# Create new building container
NEWCONTAINER=$(
    buildah from scratch
)
echo ${NEWCONTAINER}

# Find the location for rootfs for the building container
SCRATCHMNT=$(
    buildah mount ${NEWCONTAINER}
)
echo ${SCRATCHMNT}

# Install base OS
podman run \
    -v ${SCRATCHMNT}:/mnt:rw \
    registry.fedoraproject.org/fedora \
    bash -c \
    """
        dnf install \
            --installroot /mnt \
            bash coreutils shadow-utils fuse-overlayfs buildah podman skopeo \
            --exclude container-selinux \
            --releasever ${VERSION_ID} \
            --setopt=tsflags=nodocs \
            --setopt=install_weak_deps=False \
            --setopt=override_install_langs=en_US.utf8 -y \
            && dnf clean all
    """

# Clean cache, logs after the installation
echo "Cleaning cache"
if [ -d ${SCRATCHMNT} ]
    then rm -rf \
        ${SCRATCHMNT}/var/cache \
        ${SCRATCHMNT}/var/log/dnf* \
        ${SCRATCHMNT}/var/log/yum.*
fi

# Add a normal user inside the building container
echo "Adding build user"
buildah run ${NEWCONTAINER} -- /usr/bin/type -a useradd
buildah run ${NEWCONTAINER} -- bash -c "/usr/sbin/useradd build"

# Copy container setting to the container
cp containers.conf ${SCRATCHMNT}/etc/containers/
chmod 644 ${SCRATCHMNT}/etc/containers/containers.conf
if [ -f ${SCRATCHMNT}/etc/containers/storage.conf ]
    then sed \
        -e 's|^#mount_program|mount_program|g' \
        -e '/additionalimage.*/a "/var/lib/shared",' \
        -e 's|^mountopt[[:space:]]*=.*$|mountopt = "nodev,fsync=0"|g' \
        -i ${SCRATCHMNT}/etc/containers/storage.conf
fi

# Finalize settings
mkdir -p \
    ${SCRATCHMNT}/var/lib/shared/overlay-images \
    ${SCRATCHMNT}/var/lib/shared/overlay-layers

touch ${SCRATCHMNT}/var/lib/shared/overlay-images/images.lock
touch ${SCRATCHMNT}/var/lib/shared/overlay-layers/layers.lock

echo build:100000:65536 > ${SCRATCHMNT}/etc/subuid
echo build:100000:65536 > ${SCRATCHMNT}/etc/subgid

buildah config --env _CONTAINERS_USERNS_CONFIGURED="" ${NEWCONTAINER}
buildah config --env BUILDAH_ISOLATION=chroot ${NEWCONTAINER}
buildah config --user build ${NEWCONTAINER}
buildah config --workingdir /home/build ${NEWCONTAINER}
buildah config --cmd /bin/bash ${NEWCONTAINER}
buildah config --label name=${CI_PROJECT_NAME} ${NEWCONTAINER}

# Unmount the container' image
buildah unmount ${NEWCONTAINER}

# Commit the image
buildah commit ${NEWCONTAINER} ${CI_PROJECT_NAME}

# Test the image
echo "Test the image"
podman run ${CI_PROJECT_NAME} bash -c \
    """
        id \
        && pwd \
        && buildah --version \
        && podman --version \
        && skopeo --version \
        || exit
    """ || exit

# If image passes tests renew it
buildah tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:${VERSION_ID}
buildah tag ${CI_PROJECT_NAME} ${CI_REGISTRY_IMAGE}:latest
podman images
echo "Login to Gitlab registry"
echo ${CI_JOB_TOKEN} \
    | buildah login \
    -u ${CI_REGISTRY_USER} \
    --password-stdin ${CI_REGISTRY}
echo "Publish images to Gitlab registry"
buildah push ${CI_REGISTRY_IMAGE}:${VERSION_ID}
buildah push ${CI_REGISTRY_IMAGE}:latest
